%filenames for tones
filetone.hz4  = 'Tone Component 256Hz Carrier 4Hz Isochronic 6min 033s.wav';
filetone.hz8  = 'Tone Component 256Hz Carrier 8Hz Isochronic 6min 2p841s.wav';
filetone.hz10 = 'Tone Component 256Hz Carrier 10Hz Isochronic 6min.wav';
filetone.hz12 = 'Tone Component 256Hz Carrier 12Hz Isochronic 6min 2p841s.wav';

keySet =   {4,8,10,12};
valueSet = {filetone.hz4, filetone.hz8, filetone.hz10, filetone.hz12};
tonefilenames = containers.Map(keySet,valueSet);

prompt={'Enter the subject id (3 initials, data MMDDYYYY)'}; %f- a b c d, then a b c

name='Subject Name';
numlines=1;
defaultanswer={'NoIDGiven'};

beep();pause(.2);beep();pause(.2);beep();
answer=inputdlg(prompt,name,numlines,defaultanswer);
subjectid = answer{1};

%configure labstreaminglayer streams
lib = lsl_loadlib();
info = lsl_streaminfo(lib,'EventStream','Markers',1,0,'cf_string','myuniquesourceid23443'); %this is an event marker stream
outlet = lsl_outlet(info);

%prompt user to start lab recorder
msgstr = sprintf('Subject ID set to ''%s''.\n\nPlease start LabRecorder, verify that EventStream is \ndetected, then press OK.', subjectid);
uiwait(msgbox(msgstr,'Start LabRecorder','modal'));

tones = [4 8 10 12]; %tones
s = RandStream.create('mt19937ar','seed',sum(100*clock));
RandStream.setGlobalStream(s);
tones = tones(randperm(length(tones))); %randomize tones for this subject
toneconditions = 'abcd';
disp(tones);

for i = 1:length(tones)
    msgstr = sprintf('Running Tone %s (%d Hz) \n', toneconditions(i), tones(i));
    uiwait(msgbox(msgstr,'Start LabRecorder','modal'));
    
    currCondition = cat(2, 'Cond f',toneconditions(i), 'a'); 
    outlet.push_sample({currCondition},0);
    filename = cat(2,subjectid, 'f',toneconditions(i), 'a');
    keytest2(outlet);
    save(filename, 'sequence', 'times','tones');
    sequence = []; times = [];
    
    msgstr = sprintf('Done with %s, press ok to proceed to next session.\n(Put headphones on)', currCondition);
    uiwait(msgbox(msgstr,'Done with','modal'));
    pause(3);
    
    currCondition = cat(2, 'Cond f',toneconditions(i), 'b'); 
    outlet.push_sample({currCondition},0);
    filename = cat(2,subjectid, 'f',toneconditions(i), 'b');
    [y, Fs] = audioread(tonefilenames(tones(i)));
    playsnd = audioplayer(y, Fs); play(playsnd); fprintf('Playing tone %d...\n', tones(i));
    keytest2(outlet);
    clear playsnd;
    save(filename, 'sequence', 'times', 'tones');
    sequence = []; times = [];
    
    msgstr = sprintf('Done with %s, press ok to proceed to next session.', currCondition);
    uiwait(msgbox(msgstr,'Done with','modal'));
    pause(3);
    
    currCondition = cat(2, 'Cond f',toneconditions(i), 'c'); 
    outlet.push_sample({currCondition},0);
    filename = cat(2,subjectid, 'f',toneconditions(i), 'c');
    keytest2(outlet);
    save(filename, 'sequence', 'times', 'tones');
    sequence = []; times = [];
end