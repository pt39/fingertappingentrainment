function EventsMarkedForDelection = detectBadEvents(EEG)
    conds = unique({EEG.event.type});
    conds = conds(1:end-1);

    de = 30;
    removedShiftEventIndices = [];
    removedEventIndices = [];
    for i = 1:length(conds)
       currCond = conds{i};
       condstr = currCond(end-2:end);
       a = dir(cat(2, '*',condstr,'*mat')); a=a.name;
       a = load(a);

       dat = diff([0 a.times]);
       condIndex = find(strcmp(currCond, {EEG.event.type}));
       refZero = EEG.event(condIndex).latency;

       dat2 = diff([EEG.event(condIndex:condIndex+length(a.times)).latency]);

       rat = dat2./dat;
       badIndices = find((rat > 128 + de) | (rat < 128 - de));
       badIndices(badIndices == 1) = [];

    %    figure(3);
    %    stem(dat2);
    %    hold on
    %    stem(dat/0.0078, 'r');
    %    pause

       shiftedCondIndex = condIndex;
       removedEventIndices = [];
       while(length(badIndices)>5 )
           shiftedRemovedIndex = shiftedCondIndex + badIndices(1);
           removedIndex = condIndex + badIndices(1)-1;

           removedShiftEventIndices(end+1) = shiftedRemovedIndex;
           removedEventIndices(end+1) = removedIndex;

           shiftedCondIndex = shiftedCondIndex + 1;

           interval = condIndex:removedEventIndices(1)-1;

           for j = 1:length(removedEventIndices)-1
               interval = [interval, removedEventIndices(j)+1:removedEventIndices(j+1)-1];
           end

           lenfirstpart = length(interval);
           totallength = length(a.times);

           interval = [interval, removedEventIndices(end)+1:removedEventIndices(end)+1+totallength-lenfirstpart];

           dat2 = diff([EEG.event(interval).latency]);

           rat = dat2./dat;
    %        figure(1);
    %        hold off;
    %        stem(dat2);
    %        hold on
    %        pause
    %        stem(dat/0.0078, 'r');

           badIndices = find((rat > 128 + de) | (rat < 128 - de));
           badIndices(badIndices == 1) = [];

           try
%             disp(badIndices(1));
           catch
           end
       end
    end
    
    EventsMarkedForDelection = removedShiftEventIndices;
end