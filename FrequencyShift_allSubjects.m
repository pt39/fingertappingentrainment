subject = [];

%% EM
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'EM';
FrequencyShift;

subject(end+1).name = SubjectFolder;
subject(end).powdata = powdata;


%% FG
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'FG';
FrequencyShift;

subject(end+1).name = SubjectFolder;
subject(end).powdata = powdata;
%% LH

% excludedConditions = {};
% excludedStr = {};
% ErrorStateGPF();
% subject(endt+1).gfp = gfp;
%his has complications due to 2 xdf recordings

%% MBY
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'mby';
FrequencyShift;

subject(end+1).name = SubjectFolder;
subject(end).powdata = powdata;
%% ME
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'ME';
FrequencyShift;

subject(end+1).name = SubjectFolder;
subject(end).powdata = powdata;
%% SP
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'SP';
FrequencyShift;

subject(end+1).name = SubjectFolder;
subject(end).powdata = powdata;

%% Average
close all;

conds = { 'four','eight','ten','twelve'};
bandnames  = {'Delta', 'Theta', 'Alpha', 'Mu'};
%Pre To Stim

clear ga
ga.init = 0;

figure(1);
figure(2);
for i = 1:length(conds)
    for j = 1:length(bandnames)
         ga.PreToStim.(conds{i}).(bandnames{j}) = [];
        for k = 1:length(subject)
            ga.PreToStim.(conds{i}).(bandnames{j}) = [ ga.PreToStim.(conds{i}).(bandnames{j}); subject(k).powdata.(conds{i}).(bandnames{j}).PreToStim'];
        end
        
        figure(1);
        subplot(length(conds),length(bandnames),(i-1)*length(bandnames) + j)
        topoplot(mean(ga.PreToStim.(conds{i}).(bandnames{j})), EEG.chanlocs);
     	caxis([-10 10]);
        
        figure(2);
        subplot(length(conds),length(bandnames),(i-1)*length(bandnames) + j)
        topoplot(std(ga.PreToStim.(conds{i}).(bandnames{j})), EEG.chanlocs);
        caxis([0 10]);
    end
end

subplot(4,4,1);hold on;title('Delta');
subplot(4,4,2);hold on;title('Theta');
subplot(4,4,3);hold on;title('Alpha');
subplot(4,4,4);hold on;title('Mu');

%these don't show for some reason
subplot(4,4,1);hold on; ylabel('4');
subplot(4,4,5);hold on;ylabel('8');
subplot(4,4,9);hold on;ylabel('10');
subplot(4,4,13);hold on;ylabel('12');

figure(1)
suptitle('Subject Average % Power Change Pre To Stim');
figure(2);
suptitle('Subject Std % Power Change Pre To Stim');


figure(3);
figure(4);
for i = 1:length(conds)
    for j = 1:length(bandnames)
        ga.StimToPost.(conds{i}).(bandnames{j}) = [];
        for k = 1:length(subject)
            ga.StimToPost.(conds{i}).(bandnames{j}) = [ ga.StimToPost.(conds{i}).(bandnames{j}); subject(k).powdata.(conds{i}).(bandnames{j}).StimToPost'];
        end
        
        figure(3);
        subplot(length(conds),length(bandnames),(i-1)*length(bandnames) + j)
        topoplot(mean(ga.StimToPost.(conds{i}).(bandnames{j})), EEG.chanlocs);
     	caxis([-10 10]);
        
        figure(4);
        subplot(length(conds),length(bandnames),(i-1)*length(bandnames) + j)
        topoplot(std(ga.StimToPost.(conds{i}).(bandnames{j})), EEG.chanlocs);
        caxis([0 10]);
    end
end

subplot(4,4,1);hold on;title('Delta');
subplot(4,4,2);hold on;title('Theta');
subplot(4,4,3);hold on;title('Alpha');
subplot(4,4,4);hold on;title('Mu');

%these don't show for some reason
subplot(4,4,1);hold on; ylabel('4');
subplot(4,4,5);hold on;ylabel('8');
subplot(4,4,9);hold on;ylabel('10');
subplot(4,4,13);hold on;ylabel('12');

figure(3)
suptitle('Subject Average % Power Change Stim To Post');
figure(4);
suptitle('Subject Std % Power Change Stim To Post');
  