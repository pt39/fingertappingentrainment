eeglab
tonefreq = [10 8 4 12];
condition = 'abcd';

%sort them from low to high tone frequencies
fn = ls('eeggreg/*.xdf');
p = 'C:\Users\Administrator\Documents\MATLAB\GregResearch\eeggreg\';

currCondition = 'a';

j = find(condition == currCondition);
dfn = find(fn(:, 13) == condition(j));
bda = fn(dfn, 14);
  
strtit =  ['Condition ', condition(j)]; 
for i = 1:length(dfn)

    ifn = cat(2, p,'\', fn(dfn(i),:));
    EEG = pop_loadxdf(ifn , 'streamtype', 'EEG', 'exclude_markerstreams', {});
    EEG = pop_eegfiltnew(EEG, [], 0.5, 846, true, [], 0);
    EEG = eeg_checkset( EEG );
    EEG = pop_select( EEG,'nochannel',{'O2'});
    EEG = pop_chanedit(EEG, 'lookup','C:\\Users\\Administrator\\Documents\\MATLAB\\Libraries\\eeglab13_1_1b\\plugins\\dipfit2.2\\standard_BESA\\standard-10-5-cap385.elp');
    EEG.setname = cat(2, 'Condition ', currCondition, ', Part ', bda(i));
   [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
end
fs = EEG.srate;

edgefreqs = [8 12]; %specify alpha, etc

[B,A] = butter(4, edgefreqs/(fs/2));

prestim = ALLEEG(1);
prestim = filtfilt(B,A,double(prestim.data'))';

stim = ALLEEG(2);
stim = filtfilt(B,A,double(stim.data'))';

poststim = ALLEEG(3);
poststim = filtfilt(B,A,double(poststim.data'))';

%calculate mean global field power for each condition
l90=fs*90;
mGPFpre = mean(std(prestim));
mGPFstim = mean(std(stim));
mGPFpoststim = mean(std(poststim));

eGPFpre = std(std(prestim));
eGPFstim = std(std(stim));
eGPFpoststim = std(std(poststim));

figure
h = bar([1 2 3], [mGPFpre mGPFstim mGPFpoststim]);
d = sprintf(' (%0.2f -> %0.2f -> %0.2f)', mGPFpre, mGPFstim, mGPFpoststim);
title([strtit d]);
hold on
he=errorbar([1 2 3], [mGPFpre mGPFstim mGPFpoststim],[eGPFpre eGPFstim eGPFpoststim],'r', 'linestyle', 'none');


eeglab redraw
