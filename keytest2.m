function [sequence, times] = keytest2(outlet)
    sequence = [];
    times = []; 
    a = figure;
    set(a, 'WindowKeyPressFcn', @onKeyPress);
    fprintf('Collection Start...\n');
    tic;
        function onKeyPress(obj, event)
            outlet.push_sample({'KP'},0); % note that the string is wrapped into a cell-array
            sequence(end+1) = double(event.Key);
            times(end+1) = toc;
        end
    %patt = [a f s d a];
    pause(370);
    display('Collection Stopped');
    beep;pause(.25);beep;pause(.25);beep;pause(.25);
    close(a);
    
    assignin('base', 'sequence', sequence);
    assignin('base', 'times', times);
     
end