close all; clear all; clc
eeglab
f = ls('*mat'); a = load(f(1,:));
tonefreq = a.tones;
condition = 'abcd';
excludedConditions = '';
[tonefreq, I] =sort(tonefreq);%sort them from low to high tone frequencies
condition = condition(I);

fn = ls('*.mat');
p = pwd;
eegfilename = 'EE06132014.xdf';
rdata = {'ee '}; 

ifn = cat(2, p,'\', eegfilename);
EEG = pop_loadxdf(ifn , 'streamtype', 'EEG', 'exclude_markerstreams', {});
EEG = pop_chanedit(EEG, 'lookup','..\\standard-10-5-cap385.elp');
EEG.setname = 'Raw';

[ALLEEG, EEG2, CURRENTSET] = eeg_store(ALLEEG, EEG);
fs = EEG2.srate;     
last90=fs*90;
edgefreqs = [4 8;8 13]; %specify theta, alpha, etc
fs = EEG2.srate;


for ef = 1:size(edgefreqs,1)
    [B,A] = butter(4, edgefreqs(ef,:)/(fs/2));
    EEG = EEG2;
    EEG.data = filtfilt(B,A, double(EEG2.data)')';
    EEG.setname = 'Filtered';
    [ALLEEG, EEGc, CURRENTSET] = eeg_store(ALLEEG, EEG);

    figure

    fname = cat(2, 'EEG analysis frequencies: ', num2str(edgefreqs(ef,1)), ' to ', num2str(edgefreqs(ef,2)) , ' Hz');
    set(gcf, 'Name', fname);

    for co = 1:length(condition)
        
        if(sum(condition(co) == excludedConditions)==0)
            currCondition = condition(co);
            j = find(condition == currCondition);
            dfn = find(fn(:, end-5) == condition(j));
            bda = fn(dfn, end-4);

            strtit =  ['Condition ', condition(j)]; 

            constr = cat(2, 'Cond f', condition(j), 'a');
            prestim = pop_epoch(EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, prestim);

            constr = cat(2, 'Cond f', condition(j), 'b');
            stim = pop_epoch( EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, stim);

            constr = cat(2, 'Cond f', condition(j), 'c');
            poststim = pop_epoch( EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, poststim);

            prestim = prestim.data;
            stim = stim.data;
            poststim = poststim.data;

            mGPFpre = mean(std(prestim));
            mGPFstim = mean(std(stim));
            mGPFpoststim = mean(std(poststim));

            eGPFpre = std(std(prestim));
            eGPFstim = std(std(stim));
            eGPFpoststim = std(std(poststim));

            subplot(1,length(condition), co);
            h = bar([1 2 3], [mGPFpre mGPFstim mGPFpoststim], 0.2, 'facecolor', [0.4 0.6 0.8]);
            d = sprintf(' (%d Hz) (%0.2f -> %0.2f -> %0.2f)', tonefreq(co), mGPFpre, mGPFstim, mGPFpoststim);
            ca = axis;
            ca(3) = 0;
            ca(4) = 6;
            axis(ca);
            title([strtit d]);
            hold on
            he=errorbar([1 2 3], [mGPFpre mGPFstim mGPFpoststim],[eGPFpre eGPFstim eGPFpoststim],'k', 'linestyle', 'none');
            
            percentIncrease =  (mGPFstim - mGPFpre)/mGPFpre;
            percentDecrease =  (mGPFpoststim - mGPFstim)/mGPFstim;
            rdata{end + 1} = percentIncrease*100;
            rdata{end + 1} = percentDecrease*100;
        end

    end
end
save('rdata','rdata');
eeglab redraw
