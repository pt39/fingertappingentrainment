function [a b c]= createfit(errortimes,totalerrors)
%CREATEFIT    Create plot of datasets and fits
%   CREATEFIT(ERRORTIMES,TOTALERRORS)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1


% Data from dataset "totalerrors vs. errortimes":
%    X = errortimes:
%    Y = totalerrors:
%    Unweighted
%
% This function was automatically generated on 13-Mar-2014 15:15:51

% Set up figure to receive datasets and fits
f_ = clf;
figure(f_);
set(f_,'Units','Pixels','Position',[852 241 680 484]);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = axes;
set(ax_,'Units','normalized','OuterPosition',[0 0 1 1]);
set(ax_,'Box','on');
axes(ax_); hold on;


% --- Plot data originally in dataset "totalerrors vs. errortimes"
errortimes = errortimes(:);
totalerrors = totalerrors(:);
h_ = line(errortimes,totalerrors,'Parent',ax_,'Color',[0.333333 0 0.666667],...
    'LineStyle','none', 'LineWidth',1,...
    'Marker','.', 'MarkerSize',12);
xlim_(1) = min(xlim_(1),min(errortimes));
xlim_(2) = max(xlim_(2),max(errortimes));
legh_(end+1) = h_;
legt_{end+1} = 'totalerrors vs. errortimes';

% Nudge axis limits beyond data limits
if all(isfinite(xlim_))
    xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
    set(ax_,'XLim',xlim_)
else
    set(ax_, 'XLim',[39.293944624322833, 349.42102583865869]);
end


% --- Create fit "fit 1"
ok_ = isfinite(errortimes) & isfinite(totalerrors);
if ~all( ok_ )
    warning( 'GenerateMFile:IgnoringNansAndInfs', ...
        'Ignoring NaNs and Infs in data' );
end
st_ = [0 0 10 ];
ft_ = fittype('c/(1+exp(-(a*x+b)))',...
    'dependent',{'y'},'independent',{'x'},...
    'coefficients',{'a', 'b', 'c'});

% Fit this model using new data
cf_ = fit(errortimes(ok_),totalerrors(ok_),ft_,'Startpoint',st_);
a = cf_.a;
b = cf_.b;
c = cf_.c;
% Or use coefficients from the original fit:
if 0
    cv_ = { 0.01698215229017214, -2.0359017963556916, 7.6859144733456795};
    cf_ = cfit(ft_,cv_{:});
end

% Plot this fit
h_ = plot(cf_,'fit',0.95);
legend off;  % turn off legend from plot method call
set(h_(1),'Color',[1 0 0],...
    'LineStyle','-', 'LineWidth',2,...
    'Marker','none', 'MarkerSize',6);
legh_(end+1) = h_(1);
legt_{end+1} = 'fit 1';

% Done plotting data and fits.  Now finish up loose ends.
hold off;
leginfo_ = {'Orientation', 'vertical', 'Location', 'NorthEast'};
h_ = legend(ax_,legh_,legt_,leginfo_{:});  % create legend
set(h_,'Interpreter','none');
xlabel(ax_,'');               % remove x label
ylabel(ax_,'');               % remove y label
