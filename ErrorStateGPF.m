cd(SubjectFolder)
addpath('..');
eeglab

[~, subjectID, ~] = fileparts(pwd);

%     excludedConditions = {};
%     excludedStr = {};
f = dir('*f*mat'); a = load(f(1).name);
tonefreq = a.tones;
condition = 'abcd';
[tonefreq, I] =sort(tonefreq);%sort them from low to high tone frequencies
condition = condition(I);

p = pwd;

eegfilename = dir('*xdf');
eegfilename = eegfilename.name;

ifn = cat(2, p,'\', eegfilename);
EEG = pop_loadxdf(ifn , 'streamtype', 'EEG', 'exclude_markerstreams', {});
EEG = pop_rmbase(EEG, []);
EEG = pop_chanedit(EEG, 'lookup','..\standard-10-5-cap385.elp');
EEG.setname = 'Raw';
[ALLEEG, EEG2, CURRENTSET] = eeg_store(ALLEEG, EEG);

%get filter eeg recordings.
alpha = [8 13];
theta = [4 8];
fs = EEG2.srate;

[B.theta,A.theta] = butter(4, theta./(fs/2));
[B.alpha,A.alpha] = butter(4, alpha./(fs/2));

EEG_alpha = EEG2;
EEG_alpha.data = filtfilt(B.alpha,A.alpha, double(EEG2.data)')';

EEG_theta= EEG2;
EEG_theta.data = filtfilt(B.theta,A.theta, double(EEG2.data)')';

%see if there is any difference in number of events between mat file and
%eeg recording
totEvents = 0;
for i = 1:length(f)
   a = load(f(i).name); 
   totEvents = totEvents + length(a.sequence);
end
fprintf('Total number of events from MATLAB: %d\n', totEvents);
fprintf('Total number of events from EEG: %d\n', length(EEG.event)-length(unique({EEG.event.type}))+1);

%remove undesired events
evts = detectBadEvents(EEG);
EEG2 = pop_editeventvals(EEG,'delete',evts);
evts = detectBadEvents(EEG2);
disp(evts);

%compute gpf
conds = unique({EEG.event.type});
conds = conds(1:end-1);

for i = 1:12
   currCond = conds{i}; 
   condstr = currCond(end-2:end);

   fprintf('%s\n', condstr);

   %modify sequence for 'bad' conditions
   badcond = false;
   for ec = 1:length(excludedConditions)
    if(strcmp(condstr, excludedConditions(ec)))
      fprintf('\texcluded\n');
      badcond=true;
      break
    end
   end

   %get error indices from mat file
   a = dir(cat(2, '*',condstr,'*mat')); a=a.name;
   a = load(a);
   if(badcond)
       [b,e] = getErrorStartAndEnd(excludedStr{ec}, a.sequence, a.times);
   else
       [b,e] = getErrorStartAndEnd('afsdads', a.sequence, a.times);
   end

   %get corresponding indicies from eeg
    condIndex = find(strcmp(currCond, {EEG2.event.type}));
    refZero = EEG2.event(condIndex).latency;

    %eeg latencies are stored as samples
    eegLatencies = [];
    for j =1:length(b)
        eegLatencies = [eegLatencies, EEG2.event(b(j):e(j)).latency];
    end
%     disp(eegLatencies);

    %calculate gfp
    gfp.tones = a.tones;
    gfp.(condstr).alpha = EEG_alpha.data(:,int32(eegLatencies));
    gfp.(condstr).alpha = std(gfp.(condstr).alpha);
    gfp.(condstr).alpha = mean(gfp.(condstr).alpha);

    gfp.(condstr).theta = EEG_theta.data(:,int32(eegLatencies));
    gfp.(condstr).theta = std(gfp.(condstr).theta);
    gfp.(condstr).theta = mean(gfp.(condstr).theta);

end

condMatrix = ['faa';'fab';'fac';'fba';'fbb';'fbc';'fca';'fcb';'fcc';'fda';'fdb';'fdc']; 
stimTypes = 'abcd';
[~, I] = sort(a.tones);
stimTypes = stimTypes(I);

%plot raw values

for i=1:4
   currStim = stimTypes(I(i));

   rs = cat(2, 'f', currStim);

   subplot(2,4,i);
   bar([gfp.([rs, 'a']).alpha , gfp.([rs, 'b']).alpha , gfp.([rs, 'c']).alpha]);
   ylim([0, 10]); 
   set(gca, 'XtickLabel', {'Pre', 'Stim', 'Post'});
   title(a.tones(I(i)));

   subplot(2,4,4+i);
   bar([gfp.([rs, 'a']).theta , gfp.([rs, 'b']).theta , gfp.([rs, 'c']).theta]);
   ylim([0, 10]); 
   set(gca, 'XtickLabel', {'Pre', 'Stim', 'Post'});
   
   
end

subplot(2,4,1);
ylabel('Alpha');

subplot(2,4,5);
ylabel('Theta');

figure(gcf)
suptitle(['Subject ' ,SubjectFolder, ' GFP']);


%for percent change
figure;

for i=1:4
   currStim = stimTypes(I(i));

   rs = cat(2, 'f', currStim);

   gfp.([rs, 'PreToStim']).alpha = ( gfp.([rs, 'b']).alpha - gfp.([rs, 'a']).alpha )/gfp.([rs, 'a']).alpha*100;
   gfp.([rs, 'StimToPost']).alpha = ( gfp.([rs, 'c']).alpha - gfp.([rs, 'b']).alpha )/gfp.([rs, 'b']).alpha*100;
   
   gfp.([rs, 'PreToStim']).theta = ( gfp.([rs, 'b']).theta - gfp.([rs, 'a']).theta )/gfp.([rs, 'a']).theta*100;
   gfp.([rs, 'StimToPost']).theta = ( gfp.([rs, 'c']).theta- gfp.([rs, 'b']).theta )/gfp.([rs, 'b']).theta*100;
   
   subplot(2,4,i);
   bar([gfp.([rs, 'PreToStim']).alpha ,  gfp.([rs, 'StimToPost']).alpha]);
   ylim([-100, 100]); 
   set(gca, 'XtickLabel', {'Pre>Stim', 'Stim>Post'});
   title(a.tones(I(i)));

   subplot(2,4,4+i);
   bar([gfp.([rs, 'PreToStim']).theta ,  gfp.([rs, 'StimToPost']).theta]);
   ylim([-100, 100]); 
   set(gca, 'XtickLabel', {'Pre>Stim', 'Stim>Post'});
   
end

subplot(2,4,1);
ylabel('Alpha');

subplot(2,4,5);
ylabel('Theta');

figure(gcf)
suptitle(['Subject ' ,SubjectFolder, ' GFP Percentage Change (%)']);

cd('..');