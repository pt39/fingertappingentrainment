cd(SubjectFolder)
addpath('..');
eeglab

[~, subjectID, ~] = fileparts(pwd);

%     excludedConditions = {};
%     excludedStr = {};
f = dir('*f*mat'); a = load(f(1).name);
fn = ls('*f*mat');
tonefreq = a.tones;
condition = 'abcd';
[tonefreq, I] =sort(tonefreq);%sort them from low to high tone frequencies
condition = condition(I);

p = pwd;

eegfilename = dir('*xdf');
eegfilename = eegfilename.name;

ifn = cat(2, p,'\', eegfilename);
EEG = pop_loadxdf(ifn , 'streamtype', 'EEG', 'exclude_markerstreams', {});
EEG = pop_rmbase(EEG, []);
EEG = pop_chanedit(EEG, 'lookup','..\standard-10-5-cap385.elp');
EEG.setname = 'Raw';
[ALLEEG, EEG2, CURRENTSET] = eeg_store(ALLEEG, EEG);

fs = EEG2.srate;     
last90=fs*90;
edgefreqs = [0.1 3;4 7;8 13; 8 15];

bandnames = {'Delta', 'Theta', 'Alpha', 'Mu'};
conds = {'four', 'eight', 'ten', 'twelve'};

fs = EEG2.srate;

powdata.name = SubjectFolder;
for ef = 1:size(edgefreqs,1)
    [B,A] = butter(4, edgefreqs(ef,:)/(fs/2));
    EEG = EEG2;
    EEG.data = filtfilt(B,A, double(EEG2.data)')';
    EEG.setname = 'Filtered';
    [ALLEEG, EEGc, CURRENTSET] = eeg_store(ALLEEG, EEG);
    
  
    for co = 1:length(condition)
        
            currCondition = condition(co);
            j = find(condition == currCondition);
            dfn = find(fn(:, end-5) == condition(j));
            bda = fn(dfn, end-4);

            strtit =  ['Condition ', condition(j)]; 

            constr = cat(2, 'Cond f', condition(j), 'a');
            prestim = pop_epoch(EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, prestim);

            constr = cat(2, 'Cond f', condition(j), 'b');
            stim = pop_epoch( EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, stim);

            constr = cat(2, 'Cond f', condition(j), 'c');
            poststim = pop_epoch( EEGc, {constr}, [0  375], 'newname', constr, 'epochinfo', 'yes');
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, poststim);

            prestim = prestim.data;
            stim = stim.data;
            poststim = poststim.data;
            
           [prestim.spectra, prestim.freqs] = spectopo(prestim,0, EEG.srate, 'plot', 'off');
           [stim.spectra, stim.freqs] = spectopo(stim,0, EEG.srate, 'plot', 'off');
           [poststim.spectra, poststim.freqs] = spectopo(poststim,0, EEG.srate, 'plot', 'off');
            
           fc = tonefreq(co);
           
           powPre = prestim.spectra( prestim.freqs >= edgefreqs(ef,1) & prestim.freqs <= edgefreqs(ef,2)) ;
           powPre = mean(powPre,2);
           
           powStim = stim.spectra( stim.freqs >= edgefreqs(ef,1) & stim.freqs <= edgefreqs(ef,2)) ;
           powStim = mean(powStim,2);
           
           powPost = poststim.spectra( poststim.freqs >= edgefreqs(ef,1) & poststim.freqs <= edgefreqs(ef,2)) ;
           powPost = mean(powPost,2);
           
           powdata.(conds{co}).(bandnames{ef}).PreToStim = (powStim - powPre)./powPre * 100;
           powdata.(conds{co}).(bandnames{ef}).StimToPost = (powPost - powStim)./powStim * 100;

    end
end

%% make plots

%pre to stim
figure;
for i = 1:length(conds)
    for j = 1:length(bandnames)
        subplot(length(conds), length(bandnames), (i-1)*length(bandnames) + j);
        topoplot(powdata.(conds{i}).(bandnames{j}).PreToStim, EEG.chanlocs); 
        caxis([-10 10]);
    end
end

subplot(4,4,1);hold on;title('Delta');
subplot(4,4,2);hold on;title('Theta');
subplot(4,4,3);hold on;title('Alpha');
subplot(4,4,4);hold on;title('Mu');

%these don't show for some reason
subplot(4,4,1);hold on; ylabel('4');
subplot(4,4,5);hold on;ylabel('8');
subplot(4,4,9);hold on;ylabel('10');
subplot(4,4,13);hold on;ylabel('12');

figure(gcf);
supstr = cat(2, 'Subject ', SubjectFolder, ' Pre to Stim');
suptitle(supstr);

%stim to post
figure;
for i = 1:length(conds)
    for j = 1:length(bandnames)
        subplot(length(conds), length(bandnames), (i-1)*length(bandnames) + j);
        topoplot(powdata.(conds{i}).(bandnames{j}).StimToPost, EEG.chanlocs); 
        caxis([-10 10]);
    end
end

subplot(4,4,1);hold on;title('Delta');
subplot(4,4,2);hold on;title('Theta');
subplot(4,4,3);hold on;title('Alpha');
subplot(4,4,4);hold on;title('Mu');

%these don't show for some reason
subplot(4,4,1);hold on; ylabel('4');
subplot(4,4,5);hold on;ylabel('8');
subplot(4,4,9);hold on;ylabel('10');
subplot(4,4,13);hold on;ylabel('12');

figure(gcf);
supstr = cat(2, 'Subject ', SubjectFolder, ' Stim to Post');
suptitle(supstr);


cd('..');