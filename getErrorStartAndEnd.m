function [b, e] = getErrorStartAndEnd(str, fullstr, timeseq)

    % str='afsdads';

    % fullstr=sequence;
    fullstr(fullstr=='z')='a';
    % timeseq = times;
    indices = [];

    nfullstr = fullstr;
    sequencesfound = 0;
    globalstart=0;
    while(~isempty(strfind(nfullstr,str)))
        cindices = strfind(nfullstr, str);
        nfullstr =  nfullstr((cindices(1)+length(str)):end);
        indices(end+1) = globalstart + cindices(1);
        globalstart = globalstart + cindices(1)+ length(str)-1; 
        sequencesfound = sequencesfound + 1;
    end


    idealnumsequences = floor(length(fullstr)/length(str));
    accuracy = (sequencesfound/idealnumsequences)*100;

    sequencetimes = [];
    for i=1:length(indices)
        t1 = timeseq(indices(i));
        t2 = timeseq(indices(i)+length(str)-1);
        sequencetimes(end+1) = t2-t1;
    end

    keystroketimes = [];
    for i=1:length(indices)
        for j = 1:(length(str)-1)
            t1 = timeseq(indices(i)+j-1);
            t2 = timeseq(indices(i)+j);
            keystroketimes(end+1) = t2-t1;
        end
    end

    % detection of error states
    init = indices(1) + length(str);
    state = zeros(size(fullstr));
    state(init-length(str):init-1) = 1;

    currInd = 1;%init;
    
    templateIndex=1;
    while(currInd <= length(fullstr))
       currCharac = char(fullstr(currInd));
       desiredCharac = str(templateIndex);
       if(currCharac == desiredCharac)
          state(currInd) = 1;
          templateIndex = templateIndex +1;
          if(templateIndex > length(str)), templateIndex=1; end
       else
           I = find(indices > currInd);
           if(isempty(I)), break; end
           I = indices(I(1)) + length(str);
           state(I-length(str):I-1) = 1;
           templateIndex = 1;
       end
       currInd = currInd +1;
    end
%     disp(state);
    fstate = [1, state];
    bstate = [state, -1];

    I=find((fstate == 1) & (bstate ==0));
    I2=find((fstate == 0) & (bstate ==1))-1; %error ending time
    
    errortimes = timeseq(I);
    errorendingtimes = timeseq(I2);

    if(length(errortimes)> length(errorendingtimes))
        errorendingtimes(end+1) = 370;
        I2(end+1) = length(fstate);
    end
    if(length(errortimes)< length(errorendingtimes))
        errortimes = [0 errortimes] ;
        I = [1 I];
    end
    
    b=I;
    e=I2;

    errorlengths = errorendingtimes-errortimes;
%     disp(errorlengths);
    totalerrors = 1:length(errortimes);
    normtotalerrors =totalerrors/max(totalerrors);

end