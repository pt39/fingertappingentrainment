function [an, bn] = createfitnorm(errortimes,normtotalerrors, timeseq, errorendingtimes)
%CREATEFIT    Create plot of datasets and fits
%   CREATEFIT(ERRORTIMES,NORMTOTALERRORS)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1


% Data from dataset "normtotalerrors vs. errortimes":
%    X = errortimes:
%    Y = normtotalerrors:
%    Unweighted
%
% This function was automatically generated on 13-Mar-2014 15:56:34

% Set up figure to receive datasets and fits
f_ = clf;
figure(f_);
set(f_,'Units','Pixels','Position',[949 279 680 484]);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = axes;
set(ax_,'Units','normalized','OuterPosition',[0 0 1 1]);
set(ax_,'Box','on');
axes(ax_); hold on;


% --- Plot data originally in dataset "normtotalerrors vs. errortimes"
errortimes = errortimes(:);
normtotalerrors = normtotalerrors(:);

% --- Create fit "fit 1"
ok_ = isfinite(errortimes) & isfinite(normtotalerrors);
if ~all( ok_ )
    warning( 'GenerateMFile:IgnoringNansAndInfs', ...
        'Ignoring NaNs and Infs in data' );
end
st_ = [0 0 ];
ft_ = fittype('1/(1+exp(-(a*x+b)))',...
    'dependent',{'y'},'independent',{'x'},...
    'coefficients',{'a', 'b'});

% Fit this model using new data
cf_ = fit(errortimes(ok_),normtotalerrors(ok_),ft_,'Startpoint',st_);

an = cf_.a;
bn = cf_.b;

% Or use coefficients from the original fit:
if 0
    cv_ = { 0.016523366109423926, -2.8335792899450833};
    cf_ = cfit(ft_,cv_{:});
end

% Plot this fit
%matlab generated code
% h_ = plot(cf_,'fit',0.95);
% legend off;  % turn off legend from plot method call
% set(h_(1),'Color',[1 0 0],...
%     'LineStyle','-', 'LineWidth',2,...
%     'Marker','none', 'MarkerSize',6);
% legh_(end+1) = h_(1);
% legt_{end+1} = 'fit 1';

[AX, h1, h2] = plotyy(timeseq,diff([0 timeseq]), timeseq,1./(1+exp(-(cf_.a*timeseq+cf_.b))));
set(h1, 'color', 'b');
set(h2, 'color', 'r');

set(AX,{'ycolor'},{'b';'r'})
title('Percentage of total Error States Over Time')
xlabel('Time (s)')

ylabel(AX(2),'Percentage of Total Error States (%)') % left y-axis
ylabel(AX(1),'Interval Between Keystrokes (s)') % right y-axis
ylim(AX(1), [0 1.25]);
% Done plotting data and fits.  Now finish up loose ends.

% axes(AX(2));

% h_ = line(errortimes,normtotalerrors,'Parent',AX(2),'Color','r',...
%     'LineStyle','none', 'LineWidth',1,...
%     'Marker','x', 'MarkerSize',12);


for i = 1:length(errorendingtimes)
   hs=line([errortimes(i), errorendingtimes(i)], [i i]/length(errortimes), 'color','r', 'Parent', AX(2), 'linewidth', 3);  
end
legend([hs h2], 'Error States (%)', 'Sigmoid Fit', 'location', 'northwest');
xlim(AX(1),[10 360]);
xlim(AX(2),[10 360]);

set(AX(1), 'ytick', [0 1]);
