subjectFolders = {'mby', 'LH', 'FG', 'ME', 'SP', 'EM'};
delete 'withinsubj.txt';
fid = fopen('withinsubj.txt', 'w');

fprintf(fid,'subject\tTheta4Increase\tTheta4Decrease\tTheta8Increase\tTheta8Decrease\tTheta10Increase\tTheta10Decrease\tTheta12Increase\tTheta12Decrease\tAlpha4Increase\tAlpha4Decrease\tAlpha8Increase\tAlpha8Decrease\tAlpha10Increase\tAlpha10Decrease\tAlpha12Increase\tAlpha12Decrease\n');

for i = 1:length(subjectFolders)
    fn = cat(2, subjectFolders{i}, '\rdata');
    
    rdata = load(fn);
    rdata = rdata.rdata;
    
    fprintf(fid,'%s\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\n', rdata{1},rdata{2},rdata{3},rdata{4},rdata{5},rdata{6},rdata{7},rdata{8},rdata{9},rdata{10},rdata{11},rdata{12},rdata{13},rdata{14},rdata{15},rdata{17},rdata{17});
end

fclose(fid);