subject = [];

%% EM
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'EM'; %#ok<*NASGU>
ErrorStateGPF;

subject(end+1).name = SubjectFolder;
subject(end).gfp = gfp;
%% FG
excludedConditions = {'fbc', 'fca', 'fcb', 'fcc', 'fda' , 'fdb', 'fdc'};
excludedStr = {'fasdads', 'fasdads', 'fasdads', 'fasdads', 'fasdads', 'fasdads','fasdads','fasdads'};

SubjectFolder = 'FG';
ErrorStateGPF;

subject(end+1).name = SubjectFolder;
subject(end).gfp = gfp;
%% LH

% excludedConditions = {};
% excludedStr = {};
% ErrorStateGPF();
% subject(endt+1).gfp = gfp;
%his has complications due to 2 xdf recordings

%% MBY
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'mby';
ErrorStateGPF;

subject(end+1).name = SubjectFolder;
subject(end).gfp = gfp;
%% ME
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'ME';
ErrorStateGPF;

subject(end+1).name = SubjectFolder;
subject(end).gfp = gfp;
%% SP
excludedConditions = {};
excludedStr = {};

SubjectFolder = 'SP';
ErrorStateGPF;

subject(end+1).name = SubjectFolder;
subject(end).gfp = gfp;

%% Average
clear stim
conds =fields(subject(1).gfp);
conds = conds(2:13);

prefix = 'a':'d';

stim =[];
for i=1:length(subject)
    [~, I] = sort(subject(i).gfp.tones);
    
    % alpha
    pre =[ 'f', prefix(I(1)) ];
    stim.alpha.four.pre(i) = subject(i).gfp.([pre,'a']).alpha;
    stim.alpha.four.stim(i) = subject(i).gfp.([pre,'b']).alpha;
    stim.alpha.four.post(i) = subject(i).gfp.([pre,'c']).alpha;
    
    pre =[ 'f', prefix(I(2)) ];
    stim.alpha.eight.pre(i) = subject(i).gfp.([pre,'a']).alpha;
    stim.alpha.eight.stim(i) = subject(i).gfp.([pre,'b']).alpha;
    stim.alpha.eight.post(i) = subject(i).gfp.([pre,'c']).alpha;
    
    pre =[ 'f', prefix(I(3)) ];
    stim.alpha.ten.pre(i) = subject(i).gfp.([pre,'a']).alpha;
    stim.alpha.ten.stim(i) = subject(i).gfp.([pre,'b']).alpha;
    stim.alpha.ten.post(i) = subject(i).gfp.([pre,'c']).alpha;
    
    pre =[ 'f', prefix(I(4)) ];
    stim.alpha.twelve.pre(i) = subject(i).gfp.([pre,'a']).alpha;
    stim.alpha.twelve.stim(i) = subject(i).gfp.([pre,'b']).alpha;
    stim.alpha.twelve.post(i) = subject(i).gfp.([pre,'c']).alpha;
    
    %theta
    pre =[ 'f', prefix(I(1)) ];
    stim.theta.four.pre(i) = subject(i).gfp.([pre,'a']).theta;
    stim.theta.four.stim(i) = subject(i).gfp.([pre,'b']).theta;
    stim.theta.four.post(i) = subject(i).gfp.([pre,'c']).theta;
    
    pre =[ 'f', prefix(I(2)) ];
    stim.theta.eight.pre(i) = subject(i).gfp.([pre,'a']).theta;
    stim.theta.eight.stim(i) = subject(i).gfp.([pre,'b']).theta;
    stim.theta.eight.post(i) = subject(i).gfp.([pre,'c']).theta;
    
    pre =[ 'f', prefix(I(3)) ];
    stim.theta.ten.pre(i) = subject(i).gfp.([pre,'a']).theta;
    stim.theta.ten.stim(i) = subject(i).gfp.([pre,'b']).theta;
    stim.theta.ten.post(i) = subject(i).gfp.([pre,'c']).theta;
    
    pre =[ 'f', prefix(I(4)) ];
    stim.theta.twelve.pre(i) = subject(i).gfp.([pre,'a']).theta;
    stim.theta.twelve.stim(i) = subject(i).gfp.([pre,'b']).theta;
    stim.theta.twelve.post(i) = subject(i).gfp.([pre,'c']).theta;    
end

%percentage changes
conds =fields(subject(1).gfp);
conds = conds(2:13);

prefix = 'a':'d';
for i=1:length(subject)
    [~, I] = sort(subject(i).gfp.tones);
    
    % alpha
    pre =[ 'f', prefix(I(1)) ];
    stim.alpha.four.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).alpha;
    stim.alpha.four.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).alpha;
    
    pre =[ 'f', prefix(I(2)) ];
    stim.alpha.eight.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).alpha;
    stim.alpha.eight.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).alpha;
    
    pre =[ 'f', prefix(I(3)) ];
    stim.alpha.ten.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).alpha;
    stim.alpha.ten.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).alpha;
    
    pre =[ 'f', prefix(I(4)) ];
    stim.alpha.twelve.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).alpha;
    stim.alpha.twelve.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).alpha;
    
    % theta
    pre =[ 'f', prefix(I(1)) ];
    stim.theta.four.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).theta;
    stim.theta.four.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).theta;
    
    pre =[ 'f', prefix(I(2)) ];
    stim.theta.eight.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).theta;
    stim.theta.eight.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).theta;
    
    pre =[ 'f', prefix(I(3)) ];
    stim.theta.ten.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).theta;
    stim.theta.ten.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).theta;
    
    pre =[ 'f', prefix(I(4)) ];
    stim.theta.twelve.PreToStim(i) = subject(i).gfp.([pre,'PreToStim']).theta;
    stim.theta.twelve.StimToPost(i) = subject(i).gfp.([pre,'StimToPost']).theta;
end


%% plots

%for raw gfp
condStr = {'four', 'eight', 'ten', 'twelve'};
for i = 1:length(condStr)
    %alpha
    
    subplot(2,length(condStr),i)
    
    mPre = mean(stim.alpha.(condStr{i}).pre);
    mStim = mean(stim.alpha.(condStr{i}).stim);
    mPost = mean(stim.alpha.(condStr{i}).post);
    
    sPre = std(stim.alpha.(condStr{i}).pre);
    sStim = std(stim.alpha.(condStr{i}).stim);
    sPost = std(stim.alpha.(condStr{i}).post);
    
    errorbar(1:3, [mPre, mStim, mPost], [sPre, sStim, sPost], '.k');
    hold on
    bar(1:3, [mPre, mStim, mPost]);
    ylim([0, 10]); 
    set(gca, 'XtickLabel', {'Pre', 'Stim', 'Post'});
    title(a.tones(I(i)));
    
    
    %theta
    subplot(2,4,length(condStr)+i)
    
    mPre = mean(stim.theta.(condStr{i}).pre);
    mStim = mean(stim.theta.(condStr{i}).stim);
    mPost = mean(stim.theta.(condStr{i}).post);
    
    sPre = std(stim.theta.(condStr{i}).pre);
    sStim = std(stim.theta.(condStr{i}).stim);
    sPost = std(stim.theta.(condStr{i}).post);
    
    errorbar(1:3, [mPre, mStim, mPost], [sPre, sStim, sPost],'.k');
    hold on
    bar(1:3, [mPre, mStim, mPost]);
    ylim([0, 10]); 
    set(gca, 'XtickLabel', {'Pre', 'Stim', 'Post'});
    title(a.tones(I(i)));
end

subplot(2, length(condStr), 1);
ylabel('Alpha');

subplot(2, length(condStr), 5);
ylabel('Theta');

suptitle(['GFP of Group (n=', int2str(length(stim.alpha.(condStr{i}).pre)), ')']);

%for perctenage change
figure;
for i = 1:length(condStr)
    %alpha
    
    subplot(2,length(condStr),i)
    
    mPreToStim = mean(stim.alpha.(condStr{i}).PreToStim);
    mStimToPost = mean(stim.alpha.(condStr{i}).StimToPost);
    
    sPreToStim = std(stim.alpha.(condStr{i}).PreToStim);
    sStimToPost = std(stim.alpha.(condStr{i}).StimToPost);
    
    errorbar( [mPreToStim, mStimToPost], [sPreToStim, sStimToPost], '.k');
    hold on
    bar(1:2, [mPreToStim, mStimToPost]);
    ylim([-100, 100]); 
    set(gca, 'xtick', 1:2);
    set(gca, 'XtickLabel', {'Pre>Stim', 'Stim>Post'});
    title(a.tones(I(i)));

    
    %theta
    subplot(2,4,length(condStr)+i)
    
    mPreToStim = mean(stim.theta.(condStr{i}).PreToStim);
    mStimToPost = mean(stim.theta.(condStr{i}).StimToPost);
    
    sPreToStim = std(stim.theta.(condStr{i}).PreToStim);
    sStimToPost = std(stim.theta.(condStr{i}).StimToPost);
    
    errorbar(1:2, [mPreToStim, mStimToPost], [sPreToStim, sStimToPost], '.k');
    hold on
    bar(1:2, [mPreToStim, mStimToPost]);
    ylim([-100, 100]); 
    set(gca, 'xtick', 1:2);
    set(gca, 'XtickLabel', {'Pre>Stim', 'Stim>Post'});
    title(a.tones(I(i)));
end

subplot(2, length(condStr), 1);
ylabel('Alpha');

subplot(2, length(condStr), 5);
ylabel('Theta');

suptitle(['Percent Change GFP of Group (n=', int2str(length(stim.alpha.(condStr{i}).pre)), ')']);